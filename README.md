# ansible_is_server_base

An Ansible role for setting up a default server base for my testing domain, [infected.systems](https://infected.systems).

This role **does** require my custom Ansible Vault credentials.

## Features
* Enforces UTC timezone
* Configures apt to use Tor as a transport mechanism
* Installs useful utilities packageset
* Configures useful sysctls for security hardening / performance as a VM
* Installs and enables Postfix with GMail integration for sending emails
* Installs and enables unattended upgrades (with automatic reboots by default, configurable with inventory variable)
* Installs and enables UFW with default drop for incoming (except SSH)
* Installs and configures auditd
* Installs my default SSH key and disables SSH password authentication
* Upgrades all packages on the system
* Configures the system to host a Tor hidden service exposing the OpenSSH server

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| CentOS Stream | Y | WORKING |
| Debian | Y | WORKING |
| Fedora | Y | WORKING |
| FreeBSD | Y | WORKING |
| Ubuntu | Y | WORKING |

## Future Features to Add
* Add support for deploying Splunk Universal Forwarder for log collection
* Add support for configuring systemd-timesyncd(?)
* Add comprehensive auditd ruleset
